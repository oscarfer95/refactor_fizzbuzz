var assert = require('assert');
var expect = require('chai').expect;
var fizzbuzz = require('../fizzbuzz');

describe('isFizzbuzz', function() {

    it("Return 'true' given 15", function() {
        expect(isFizzbuzz(15)).to.equal(true);
    });

    it("Return 'false' given 10", function() {
        expect(isFizzbuzz(10)).to.equal(false);
    });

});

describe('isFizz', function() {

    it("Return 'true' given 3", function() {
        expect(isFizz(3)).to.equal(true);
    });

    it("Return 'false' given 5", function() {
        expect(isFizz(5)).to.equal(false);
    });

});

describe('isBuzz', function() {

    it("Return 'true' given 5", function() {
        expect(isBuzz(5)).to.equal(true);
    });

    it("Return 'false' given 3", function() {
        expect(isBuzz(3)).to.equal(false);
    });

});

describe('print', function() {

    it("Return 'Fizzbuzz' message", function() {
        expect(print("Fizzbuzz")).to.equal("Fizzbuzz");
    });

    it("Return 'Buzz' message", function() {
        expect(print("Buzz")).to.equal("Buzz");
    });

    it("Return number '1'", function() {
        expect(print(1)).to.equal(1);
    });

});