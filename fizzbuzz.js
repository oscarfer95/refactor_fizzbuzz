function fizzbuzz(limit) {
    for (number = 0; number <= limit; number++) {
        if (isFizzbuzz(number)) {
            print("FizzBuzz");
        } else
        if (isFizz(number)) {
            print("Fizz");
        } else
        if (isBuzz(number)) {
            print("Buzz");
        } else
            print(number);
    }
}

isFizzbuzz = function(number) {
    if ((isFizz(number)) && (isBuzz(number))) {
        return true;
    } else {
        return false;
    }
}

isFizz = function(number) {
    if (number % 3 === 0) {
        return true;
    } else {
        return false;
    }
}

isBuzz = function(number) {
    if (number % 5 === 0) {
        return true;
    } else {
        return false;
    }
}

print = function(message) {
    console.log(message);
    return message;
}

fizzbuzz(100)